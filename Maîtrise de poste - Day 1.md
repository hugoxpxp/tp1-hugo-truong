# Maîtrise de poste - Day 1

### Host os

- nom de la machine

```hostname
LAPTOP-17497HSE
```

- os et versions

```
get-computerinfo
[...]
OsVersion :10.0.18363
[...]
OsName :Microsoft Windows 10 Famille

```

- architecture processeur

```
get-computerinfo
[...]
OsArchitecture    : 64 bits.
```

- quantité de ram et modèle de la marque

```
Capacity
8589934592
8589934592
```

### devices

```
get-computerinfo
CsProcessors : {Intel(R) Core(TM) i7-10750H CPU @ 2.60GHz}
```

le I7 est un indicateur qui sert a montrer les performances d'un processeur , plus le chiffre derrière le I est grand plus le processeur est performant , le 10 qui suit sert à indiquer la génération du processeur , le numéro 750 correspond au sku généralement plus le sku est grand plus le processeur à de fonctionalitées et le H signifie "Haute performance optimisée pour le mobile"

```
get-WmiObject win32_videoController
Caption : NVIDIA GeForce RTX 2060
```

- disque dur

```
get-disk

Number Friendly Name                                                                                                                      Serial Number                    HealthStatus         OperationalStatus      Total Size Partition
                                                                                                                                                                                                                                  Style
------ -------------                                                                                                                      -------------                    ------------         -----------------      ---------- ----------
0      SAMSUNG MZVLB512HBJQ-00A00                                                                                                         0025_3884_0100_41DF.             Healthy              Online                  476.94 GB GPT

```

```Get-Partition
 DiskPath : \\?\scsi#disk&ven_nvme&prod_samsung_mzvlb512#5&8196a1&0&000000#{53f56307-b6bf-11d0-94f2-00a0c91efb8b}

PartitionNumber  DriveLetter Offset                                                                                                   Size Type
---------------  ----------- ------                                                                                                   ---- ----
1                            1048576                                                                                               16.6 GB Recovery
2                            17826840576                                                                                            100 MB System
3                            17931698176                                                                                             16 MB Reserved
4                C           17948475392                                                                                         459.25 GB Basic
5                            511061262336                                                                                          1000 MB Recovery
```

Les partitions recovery servent à restaurer windows ou à régler les problèmes systèmes
la partition system contient les informations nécessaires au démarrage
la partition basic du disque dur est la ou nous allons enregistrer nos fichier/données

## users

```net user

comptes d’utilisateurs de \\LAPTOP-17497HSE

-------------------------------------------------------------------------------
Administrateur           DefaultAccount           hugox
Invité                   WDAGUtilityAccount
La commande s’est terminée correctement.
```

## Processus

```
>wmic process get description,processid,parentprocessid,commandline /format:csv

Node,CommandLine,Description,ParentProcessId,ProcessId
LAPTOP-17497HSE,,System Idle Process,0,0
LAPTOP-17497HSE,,System,0,4
LAPTOP-17497HSE,,Registry,4,144
LAPTOP-17497HSE,,smss.exe,4,556
LAPTOP-17497HSE,,csrss.exe,640,812
LAPTOP-17497HSE,,wininit.exe,640,916
LAPTOP-17497HSE,,services.exe,916,988
LAPTOP-17497HSE,,lsass.exe,916,1004
LAPTOP-17497HSE,,svchost.exe,988,608
LAPTOP-17497HSE,,svchost.exe,988,504
LAPTOP-17497HSE,,fontdrvhost.exe,916,1044
LAPTOP-17497HSE,,WUDFHost.exe,988,1052
LAPTOP-17497HSE,,svchost.exe,988,1140
LAPTOP-17497HSE,,svchost.exe,988,1260
LAPTOP-17497HSE,,WUDFHost.exe,988,1456
LAPTOP-17497HSE,,svchost.exe,988,1548
LAPTOP-17497HSE,,svchost.exe,988,1560
LAPTOP-17497HSE,,svchost.exe,988,1568
LAPTOP-17497HSE,,svchost.exe,988,1704
LAPTOP-17497HSE,,svchost.exe,988,1716
LAPTOP-17497HSE,,svchost.exe,988,1736
LAPTOP-17497HSE,,svchost.exe,988,1844
LAPTOP-17497HSE,,svchost.exe,988,1880
LAPTOP-17497HSE,,svchost.exe,988,1932
LAPTOP-17497HSE,,svchost.exe,988,1996
LAPTOP-17497HSE,,svchost.exe,988,2044
LAPTOP-17497HSE,,svchost.exe,988,2008
LAPTOP-17497HSE,,svchost.exe,988,2152
LAPTOP-17497HSE,,svchost.exe,988,2172
LAPTOP-17497HSE,,NVDisplay.Container.exe,988,2224
LAPTOP-17497HSE,,svchost.exe,988,2284
LAPTOP-17497HSE,,svchost.exe,988,2292
LAPTOP-17497HSE,,svchost.exe,988,2416
LAPTOP-17497HSE,,svchost.exe,988,2456
LAPTOP-17497HSE,,svchost.exe,988,2464
LAPTOP-17497HSE,,svchost.exe,988,2472
LAPTOP-17497HSE,,svchost.exe,988,2484
LAPTOP-17497HSE,,svchost.exe,988,2564
LAPTOP-17497HSE,,Memory Compression,4,2692
LAPTOP-17497HSE,,svchost.exe,988,2764
LAPTOP-17497HSE,,igfxCUIService.exe,988,2844
LAPTOP-17497HSE,,svchost.exe,988,2924
LAPTOP-17497HSE,,svchost.exe,988,2932
LAPTOP-17497HSE,,svchost.exe,988,2952
LAPTOP-17497HSE,,svchost.exe,988,3028
LAPTOP-17497HSE,,svchost.exe,988,3212
LAPTOP-17497HSE,,svchost.exe,988,3380
LAPTOP-17497HSE,,svchost.exe,988,3388
LAPTOP-17497HSE,,svchost.exe,988,3536
LAPTOP-17497HSE,,svchost.exe,988,3584
LAPTOP-17497HSE,,wlanext.exe,3536,3660
LAPTOP-17497HSE,,conhost.exe,3660,3728
LAPTOP-17497HSE,,spoolsv.exe,988,3736
LAPTOP-17497HSE,,svchost.exe,988,3864
LAPTOP-17497HSE,,svchost.exe,988,3908
LAPTOP-17497HSE,,IntelCpHDCPSvc.exe,988,4104
LAPTOP-17497HSE,,svchost.exe,988,4112
LAPTOP-17497HSE,,svchost.exe,988,4120
LAPTOP-17497HSE,,svchost.exe,988,4136
LAPTOP-17497HSE,,svchost.exe,988,4144
LAPTOP-17497HSE,,svchost.exe,988,4152
LAPTOP-17497HSE,,DAX3API.exe,988,4176
LAPTOP-17497HSE,,ThunderboltService.exe,988,4184
LAPTOP-17497HSE,,RtkAudUService64.exe,988,4192
LAPTOP-17497HSE,,svchost.exe,988,4200
LAPTOP-17497HSE,,GameManagerService.exe,988,4212
LAPTOP-17497HSE,,svchost.exe,988,4220
LAPTOP-17497HSE,,gameinputsvc.exe,988,4236
LAPTOP-17497HSE,,LMS.exe,988,4248
LAPTOP-17497HSE,,svchost.exe,988,4256
LAPTOP-17497HSE,,svchost.exe,988,4264
LAPTOP-17497HSE,,nvcontainer.exe,988,4272
LAPTOP-17497HSE,,RazerCentralService.exe,988,4280
LAPTOP-17497HSE,,RzSDKServer.exe,988,4288
LAPTOP-17497HSE,,MsMpEng.exe,988,4300
LAPTOP-17497HSE,,RzSDKService.exe,988,4360
LAPTOP-17497HSE,,esif_uf.exe,988,4380
LAPTOP-17497HSE,,svchost.exe,988,4400
LAPTOP-17497HSE,,svchost.exe,988,4796
LAPTOP-17497HSE,,svchost.exe,988,4960
LAPTOP-17497HSE,,jhi_service.exe,988,4488
LAPTOP-17497HSE,,svchost.exe,988,5300
LAPTOP-17497HSE,,GamingServicesNet.exe,988,5736
LAPTOP-17497HSE,,GamingServices.exe,988,5796
LAPTOP-17497HSE,,svchost.exe,988,6128
LAPTOP-17497HSE,,svchost.exe,988,6868
LAPTOP-17497HSE,,svchost.exe,988,6996
LAPTOP-17497HSE,,svchost.exe,988,6164
LAPTOP-17497HSE,,Razer Synapse Service.exe,988,7280
LAPTOP-17497HSE,,svchost.exe,988,7384
LAPTOP-17497HSE,,NisSrv.exe,988,8020
LAPTOP-17497HSE,,dllhost.exe,504,816
LAPTOP-17497HSE,,OfficeClickToRun.exe,988,7448
LAPTOP-17497HSE,,svchost.exe,988,8280
LAPTOP-17497HSE,,PresentationFontCache.exe,988,8344
LAPTOP-17497HSE,,svchost.exe,988,2872
LAPTOP-17497HSE,,svchost.exe,988,6740
LAPTOP-17497HSE,,svchost.exe,988,9496
LAPTOP-17497HSE,,GoogleCrashHandler.exe,8548,9732
LAPTOP-17497HSE,,GoogleCrashHandler64.exe,8548,9752
LAPTOP-17497HSE,,svchost.exe,988,9908
LAPTOP-17497HSE,,svchost.exe,988,10152
LAPTOP-17497HSE,,svchost.exe,988,10204
LAPTOP-17497HSE,,AppVShNotify.exe,7448,10768
LAPTOP-17497HSE,,svchost.exe,988,2196
LAPTOP-17497HSE,,svchost.exe,988,12484
LAPTOP-17497HSE,,svchost.exe,988,13232
LAPTOP-17497HSE,,svchost.exe,988,12656
LAPTOP-17497HSE,,SecurityHealthService.exe,988,12852
LAPTOP-17497HSE,,svchost.exe,988,4356
LAPTOP-17497HSE,,SearchIndexer.exe,988,14736
LAPTOP-17497HSE,,svchost.exe,988,16856
LAPTOP-17497HSE,,aesm_service.exe,988,9804
LAPTOP-17497HSE,,SgrmBroker.exe,988,9852
LAPTOP-17497HSE,,svchost.exe,988,8632
LAPTOP-17497HSE,,svchost.exe,988,19024
LAPTOP-17497HSE,,svchost.exe,988,8320
LAPTOP-17497HSE,,svchost.exe,988,15068
LAPTOP-17497HSE,,svchost.exe,988,1148
LAPTOP-17497HSE,,svchost.exe,988,15124
LAPTOP-17497HSE,,csrss.exe,16396,21272
LAPTOP-17497HSE,,winlogon.exe,16396,17512
LAPTOP-17497HSE,,fontdrvhost.exe,17512,20144
LAPTOP-17497HSE,,dwm.exe,17512,3816
LAPTOP-17497HSE,,gameinputsvc.exe,4236,8016
LAPTOP-17497HSE,,NVDisplay.Container.exe,2224,10492
LAPTOP-17497HSE,,DAX3API.exe,4176,11180
LAPTOP-17497HSE,"C:\Program Files (x86)\Razer\Synapse3\Service\..\UserProcess\Razer Synapse Service Process.exe",Razer Synapse Service Process.exe,7280,3228
LAPTOP-17497HSE,sihost.exe,sihost.exe,3028,16764
LAPTOP-17497HSE,"C:\Program Files\NVIDIA Corporation\NvContainer\nvcontainer.exe" -f "C:\ProgramData\NVIDIA\NvContainerUser%d.log" -d "C:\Program Files\NVIDIA Corporation\NvContainer\plugins\User" -r -l 3 -p 30000 -st "C:\Program Files\NVIDIA Corporation\NvContainer\NvContainerTelemetryApi.dll" -c,nvcontainer.exe,4272,12376
LAPTOP-17497HSE,C:\Windows\system32\svchost.exe -k UnistackSvcGroup -s CDPUserSvc,svchost.exe,988,5180
LAPTOP-17497HSE,C:\Windows\system32\svchost.exe -k BthAppGroup -p -s BluetoothUserService,svchost.exe,988,15504
LAPTOP-17497HSE,C:\Windows\system32\svchost.exe -k UnistackSvcGroup -s WpnUserService,svchost.exe,988,1448
LAPTOP-17497HSE,"C:\Windows\System32\DriverStore\FileRepository\cui_dch.inf_amd64_ba5b1813656e5c27\igfxEM.exe",igfxEM.exe,2844,11376
LAPTOP-17497HSE,taskhostw.exe {222A245B-E637-4AE9-A93F-A59CA119A75E},taskhostw.exe,2284,2184
LAPTOP-17497HSE,,svchost.exe,988,20096
LAPTOP-17497HSE,,taskhostw.exe,2284,12788
LAPTOP-17497HSE,C:\Windows\Explorer.EXE,explorer.exe,11608,9544
LAPTOP-17497HSE,C:\Windows\system32\svchost.exe -k ClipboardSvcGroup -p -s cbdhsvc,svchost.exe,988,20008
LAPTOP-17497HSE,C:\Windows\system32\SettingSyncHost.exe -Embedding,SettingSyncHost.exe,504,16292
LAPTOP-17497HSE,"C:\Windows\SystemApps\Microsoft.Windows.StartMenuExperienceHost_cw5n1h2txyewy\StartMenuExperienceHost.exe" -ServerName:App.AppXywbrabmsek0gm3tkwpr5kwzbs55tkqay.mca,StartMenuExperienceHost.exe,504,7716
LAPTOP-17497HSE,C:\Windows\System32\RuntimeBroker.exe -Embedding,RuntimeBroker.exe,504,16276
LAPTOP-17497HSE,C:\Windows\system32\DllHost.exe /Processid:{AB8902B4-09CA-4BB6-B78D-A8F59079A8D5},dllhost.exe,504,12816
LAPTOP-17497HSE,,RtkAudUService64.exe,4192,12192
LAPTOP-17497HSE,"C:\Windows\SystemApps\Microsoft.Windows.Cortana_cw5n1h2txyewy\SearchUI.exe" -ServerName:CortanaUI.AppXa50dqqa5gqv4a428c9y1jjw7m3btvepj.mca,SearchUI.exe,504,14332
LAPTOP-17497HSE,"C:\Program Files (x86)\NVIDIA Corporation\NvNode\NVIDIA Web Helper.exe" index.js,NVIDIA Web Helper.exe,17128,5100
LAPTOP-17497HSE,,ctfmon.exe,2872,13576
LAPTOP-17497HSE,C:\Windows\System32\RuntimeBroker.exe -Embedding,RuntimeBroker.exe,504,3412
LAPTOP-17497HSE,\??\C:\Windows\system32\conhost.exe 0x4,conhost.exe,5100,13644
LAPTOP-17497HSE,"C:\Program Files\WindowsApps\Microsoft.YourPhone_1.20101.99.0_x64__8wekyb3d8bbwe\YourPhone.exe" -ServerName:App.AppX9yct9q388jvt4h7y0gn06smzkxcsnt8m.mca,YourPhone.exe,504,5348
LAPTOP-17497HSE,C:\Windows\System32\RuntimeBroker.exe -Embedding,RuntimeBroker.exe,504,20852
LAPTOP-17497HSE,C:\Windows\System32\RuntimeBroker.exe -Embedding,RuntimeBroker.exe,504,15172
LAPTOP-17497HSE,C:\Windows\system32\DllHost.exe /Processid:{973D20D7-562D-44B9-B70B-5A0F49CCDF3F},dllhost.exe,504,20768
LAPTOP-17497HSE,"C:\Windows\System32\SecurityHealthSystray.exe" ,SecurityHealthSystray.exe,9544,20540
LAPTOP-17497HSE,"C:\Windows\System32\RtkAudUService64.exe" -background,RtkAudUService64.exe,9544,18992
LAPTOP-17497HSE,"C:\Program Files (x86)\Razer\Synapse3\WPFUI\Framework\Razer Synapse 3 Host\Razer Synapse 3.exe" ,Razer Synapse 3.exe,9544,6624
LAPTOP-17497HSE,"C:\Program Files (x86)\Razer\Razer Services\Razer Central\Razer Central.exe" /Client,Razer Central.exe,4280,10060
LAPTOP-17497HSE,CefSharp.BrowserSubprocess.exe --type=gpu-process --disable-features=AsyncWheelEvents,SurfaceSynchronization --no-sandbox --log-file="C:\ProgramData\Razer\Razer Central\Logs\cef_hugox.log" --log-severity=info --lang=en-US --cefsharpexitsub --gpu-preferences=KAAAAAAAAACAAwCAAQAAAAAAAAAAAGAAEAAAAAAAAAAAAAAAAAAAACgAAAAEAAAAIAAAAAAAAAAoAAAAAAAAADAAAAAAAAAAOAAAAAAAAAAQAAAAAAAAAAAAAAAKAAAAEAAAAAAAAAAAAAAACwAAABAAAAAAAAAAAQAAAAoAAAAQAAAAAAAAAAEAAAALAAAA --log-file="C:\ProgramData\Razer\Razer Central\Logs\cef_hugox.log" --log-severity=info --lang=en-US --cefsharpexitsub --service-request-channel-token=787A59AD225DCC006AAAD6F17382CF01 --mojo-platform-channel-handle=2116 /prefetch:2 --host-process-id=10060 --host-process-id=10060,CefSharp.BrowserSubprocess.exe,10060,14976
LAPTOP-17497HSE,"C:\Users\hugox\AppData\Local\Microsoft\OneDrive\OneDrive.exe" /background,OneDrive.exe,9544,17256
LAPTOP-17497HSE,CefSharp.BrowserSubprocess.exe --type=renderer --no-sandbox --disable-features=AsyncWheelEvents,SurfaceSynchronization --service-pipe-token=46BCDE3B850A668D08C750F977ACE60C --lang=en-US --log-file="C:\ProgramData\Razer\Razer Central\Logs\cef_hugox.log" --log-severity=info --enable-system-flash=1 --cefsharpexitsub --device-scale-factor=1.25 --num-raster-threads=4 --enable-main-frame-before-activation --service-request-channel-token=46BCDE3B850A668D08C750F977ACE60C --renderer-client-id=3 --mojo-platform-channel-handle=4240 /prefetch:1 --host-process-id=10060,CefSharp.BrowserSubprocess.exe,10060,17392
LAPTOP-17497HSE,"C:\Windows\SystemApps\ShellExperienceHost_cw5n1h2txyewy\ShellExperienceHost.exe" -ServerName:App.AppXtk181tbxbce2qsex02s8tw7hfxa9xb3t.mca,ShellExperienceHost.exe,504,3240
LAPTOP-17497HSE,C:\Windows\System32\RuntimeBroker.exe -Embedding,RuntimeBroker.exe,504,2884
LAPTOP-17497HSE,"C:\Users\hugox\AppData\Local\Discord\app-0.0.307\Discord.exe" ,Discord.exe,9544,19064
LAPTOP-17497HSE,"C:\Users\hugox\AppData\Local\Discord\app-0.0.307\Discord.exe" --type=gpu-process --field-trial-handle=1544,10577206270723331433,14271523452381717022,131072 --disable-features=SpareRendererForSitePerProcess --gpu-preferences=KAAAAAAAAADgAAAwAAAAAAAAYAAAAAAAEAAAAAAAAAAAAAAAAAAAACgAAAAEAAAAIAAAAAAAAAAoAAAAAAAAADAAAAAAAAAAOAAAAAAAAAAQAAAAAAAAAAAAAAAFAAAAEAAAAAAAAAAAAAAABgAAABAAAAAAAAAAAQAAAAUAAAAQAAAAAAAAAAEAAAAGAAAA --service-request-channel-token=3979358012215625543 --mojo-platform-channel-handle=1844 --ignored=" --type=renderer " /prefetch:2,Discord.exe,19064,15276
LAPTOP-17497HSE,"C:\Users\hugox\AppData\Local\Discord\app-0.0.307\Discord.exe" --type=utility --field-trial-handle=1544,10577206270723331433,14271523452381717022,131072 --disable-features=SpareRendererForSitePerProcess --lang=fr --service-sandbox-type=network --service-request-channel-token=6310676394260554757 --mojo-platform-channel-handle=2252 /prefetch:8,Discord.exe,19064,5612
LAPTOP-17497HSE,"C:\Program Files (x86)\Epic Games\Launcher\Portal\Binaries\Win64\EpicGamesLauncher.exe" -silent,EpicGamesLauncher.exe,9544,1664
LAPTOP-17497HSE,"C:\Users\hugox\AppData\Local\Microsoft\Teams\current\Teams.exe" --system-initiated,Teams.exe,1124,15748
LAPTOP-17497HSE,"C:/Program Files (x86)/Epic Games/Launcher/Engine/Binaries/Win64/UnrealCEFSubProcess.exe" --type=gpu-process --no-sandbox --lang=fr --locales-dir-path="C:/Program Files (x86)/Epic Games/Launcher/Engine/Binaries/ThirdParty/CEF3/Win64/Resources/locales" --log-file=C:/Users/hugox/AppData/Local/EpicGamesLauncher/Saved/Logs/cef3.log --log-severity=warning --product-version="EpicGamesLauncher/10.19.3-14663055+++Portal+Release-Live UnrealEngine/4.23.0-14663055+++Portal+Release-Live Chrome/59.0.3071.15" --resources-dir-path="C:/Program Files (x86)/Epic Games/Launcher/Engine/Binaries/ThirdParty/CEF3/Win64/Resources" --supports-dual-gpus=false --gpu-driver-bug-workarounds=7,10,19,23,41,61,74 --disable-gl-extensions="GL_KHR_blend_equation_advanced GL_KHR_blend_equation_advanced_coherent" --gpu-vendor-id=0x8086 --gpu-device-id=0x9bc4 --gpu-driver-vendor="Intel Corporation" --gpu-driver-version=26.20.100.7642 --gpu-driver-date=1-22-2020 --gpu-secondary-vendor-ids=0x10de --gpu-secondary-device-ids=0x1f15 --lang=fr --locales-dir-path="C:/Program Files (x86)/Epic Games/Launcher/Engine/Binaries/ThirdParty/CEF3/Win64/Resources/locales" --log-file=C:/Users/hugox/AppData/Local/EpicGamesLauncher/Saved/Logs/cef3.log --log-severity=warning --product-version="EpicGamesLauncher/10.19.3-14663055+++Portal+Release-Live UnrealEngine/4.23.0-14663055+++Portal+Release-Live Chrome/59.0.3071.15" --resources-dir-path="C:/Program Files (x86)/Epic Games/Launcher/Engine/Binaries/ThirdParty/CEF3/Win64/Resources" --service-request-channel-token=3674EBF573F3AD44376AE88593DD5C1B --mojo-platform-channel-handle=2152 /prefetch:2,UnrealCEFSubProcess.exe,1664,18904
LAPTOP-17497HSE,"C:\Program Files\WindowsApps\Microsoft.GamingApp_2010.1001.15.0_x64__8wekyb3d8bbwe\XboxAppServices.exe" ,XboxAppServices.exe,9544,11660
LAPTOP-17497HSE,"C:\Users\hugox\AppData\Local\Microsoft\Teams\current\Teams.exe" --type=gpu-process --field-trial-handle=1776,16690437217103656881,320088810334711641,131072 --enable-features=WebComponentsV0Enabled --disable-features=PictureInPicture,SpareRendererForSitePerProcess --electron-shared-settings=eyJjci5jb21wYW55IjoiRWxlY3Ryb24iLCJjci5kdW1wcyI6IiIsImNyLmVuYWJsZWQiOmZhbHNlLCJjci5wcm9kdWN0IjoiRWxlY3Ryb24iLCJjci5zZXNzaW9uIjoiIiwiY3IudXJsIjoiIiwiY3IudmVyc2lvbiI6IiJ9 --gpu-preferences=KAAAAAAAAADgAAAwAAAAAAAAYAAAAAAAEAAAAAAAAAAAAAAAAAAAACgAAAAEAAAAIAAAAAAAAAAoAAAAAAAAADAAAAAAAAAAOAAAAAAAAAAQAAAAAAAAAAAAAAAFAAAAEAAAAAAAAAAAAAAABgAAABAAAAAAAAAAAQAAAAUAAAAQAAAAAAAAAAEAAAAGAAAA --mojo-platform-channel-handle=1792 --ignored=" --type=renderer " /prefetch:2,Teams.exe,15748,21412
LAPTOP-17497HSE,"C:\Users\hugox\AppData\Local\Microsoft\Teams\current\Teams.exe" --type=utility --field-trial-handle=1776,16690437217103656881,320088810334711641,131072 --enable-features=WebComponentsV0Enabled --disable-features=PictureInPicture,SpareRendererForSitePerProcess --lang=fr --service-sandbox-type=network --enable-wer --electron-shared-settings=eyJjci5jb21wYW55IjoiRWxlY3Ryb24iLCJjci5kdW1wcyI6IiIsImNyLmVuYWJsZWQiOmZhbHNlLCJjci5wcm9kdWN0IjoiRWxlY3Ryb24iLCJjci5zZXNzaW9uIjoiIiwiY3IudXJsIjoiIiwiY3IudmVyc2lvbiI6IiJ9 --mojo-platform-channel-handle=2340 /prefetch:8,Teams.exe,15748,3288
LAPTOP-17497HSE,"C:\Users\hugox\AppData\Local\Microsoft\Teams\current\Teams.exe" --type=renderer --autoplay-policy=no-user-gesture-required --disable-background-timer-throttling --field-trial-handle=1776,16690437217103656881,320088810334711641,131072 --enable-features=WebComponentsV0Enabled --disable-features=PictureInPicture,SpareRendererForSitePerProcess --lang=fr --enable-wer --app-user-model-id=com.squirrel.Teams.Teams --app-path="C:\Users\hugox\AppData\Local\Microsoft\Teams\current\resources\app.asar" --enable-sandbox --native-window-open --preload="C:\Users\hugox\AppData\Local\Microsoft\Teams\current\resources\app.asar\lib\renderer\notifications\preload_notifications_sandbox.js" --background-color=#fff --enable-websql --electron-shared-settings=eyJjci5jb21wYW55IjoiRWxlY3Ryb24iLCJjci5kdW1wcyI6IiIsImNyLmVuYWJsZWQiOmZhbHNlLCJjci5wcm9kdWN0IjoiRWxlY3Ryb24iLCJjci5zZXNzaW9uIjoiIiwiY3IudXJsIjoiIiwiY3IudmVyc2lvbiI6IiJ9 --device-scale-factor=1.25 --num-raster-threads=4 --enable-main-frame-before-activation --renderer-client-id=9 --no-v8-untrusted-code-mitigations --mojo-platform-channel-handle=2580 /prefetch:1 --msteams-process-type=notificationsManager,Teams.exe,15748,6660
LAPTOP-17497HSE,C:\Windows\system32\ApplicationFrameHost.exe -Embedding,ApplicationFrameHost.exe,504,17160
LAPTOP-17497HSE,"C:\Users\hugox\AppData\Local\Microsoft\Teams\current\Teams.exe" --type=renderer --autoplay-policy=no-user-gesture-required --disable-background-timer-throttling --field-trial-handle=1776,16690437217103656881,320088810334711641,131072 --enable-features=WebComponentsV0Enabled --disable-features=PictureInPicture,SpareRendererForSitePerProcess --lang=fr --enable-wer --app-user-model-id=com.squirrel.Teams.Teams --app-path="C:\Users\hugox\AppData\Local\Microsoft\Teams\current\resources\app.asar" --webview-tag --no-sandbox --no-zygote --native-window-open --preload="C:\Users\hugox\AppData\Local\Microsoft\Teams\current\resources\app.asar\lib\renderer\preload.js" --background-color=#fff --enable-websql --electron-shared-settings=eyJjci5jb21wYW55IjoiRWxlY3Ryb24iLCJjci5kdW1wcyI6IiIsImNyLmVuYWJsZWQiOmZhbHNlLCJjci5wcm9kdWN0IjoiRWxlY3Ryb24iLCJjci5zZXNzaW9uIjoiIiwiY3IudXJsIjoiIiwiY3IudmVyc2lvbiI6IiJ9 --device-scale-factor=1.25 --num-raster-threads=4 --enable-main-frame-before-activation --renderer-client-id=10 --no-v8-untrusted-code-mitigations --mojo-platform-channel-handle=3372 /prefetch:1 --msteams-process-type=mainWindow,Teams.exe,15748,11980
LAPTOP-17497HSE,C:\Users\hugox\AppData\Local\Discord\app-0.0.307\Discord.exe --no-rate-limit --no-upload-gzip --type=crash-handler "--crashes-directory=C:\Users\hugox\AppData\Local\Temp\Discord Crashes" "--database=C:\Users\hugox\AppData\Local\Temp\Discord Crashes" "--metrics-dir=C:\Users\hugox\AppData\Local\Temp\Discord Crashes" --url=https://sentry.io/api/146342/minidump/?sentry_key=384ce4413de74fe0be270abe03b2b35a --initial-client-data=0xabc,0xac0,0xae0,0xb48,0xb40,0x613f090,0x613f0a0,0x613f0ac,Discord.exe,19064,3036
LAPTOP-17497HSE,"C:\Users\hugox\AppData\Local\Microsoft\Teams\current\Teams.exe" --type=utility --field-trial-handle=1776,16690437217103656881,320088810334711641,131072 --enable-features=WebComponentsV0Enabled --disable-features=PictureInPicture,SpareRendererForSitePerProcess --lang=fr --service-sandbox-type=audio --enable-wer --electron-shared-settings=eyJjci5jb21wYW55IjoiRWxlY3Ryb24iLCJjci5kdW1wcyI6IiIsImNyLmVuYWJsZWQiOmZhbHNlLCJjci5wcm9kdWN0IjoiRWxlY3Ryb24iLCJjci5zZXNzaW9uIjoiIiwiY3IudXJsIjoiIiwiY3IudmVyc2lvbiI6IiJ9 --mojo-platform-channel-handle=3596 /prefetch:8,Teams.exe,15748,14192
LAPTOP-17497HSE,Spotify.exe --autostart,Spotify.exe,19128,7220
LAPTOP-17497HSE,"C:\Users\hugox\AppData\Local\Discord\app-0.0.307\Discord.exe" --type=renderer --autoplay-policy=no-user-gesture-required --field-trial-handle=1544,10577206270723331433,14271523452381717022,131072 --disable-features=SpareRendererForSitePerProcess --lang=fr --app-user-model-id=com.squirrel.Discord.Discord --app-path="C:\Users\hugox\AppData\Local\Discord\app-0.0.307\resources\app.asar" --no-sandbox --no-zygote --native-window-open --preload="C:\Users\hugox\AppData\Roaming\discord\0.0.307\modules\discord_desktop_core\core.asar\app\mainScreenPreload.js" --disable-remote-module --context-isolation --background-color=#202225 --enable-websql --device-scale-factor=1.25 --num-raster-threads=4 --enable-main-frame-before-activation --service-request-channel-token=17363507809605341892 --renderer-client-id=7 --no-v8-untrusted-code-mitigations --mojo-platform-channel-handle=2876 /prefetch:1 --enable-node-leakage-in-renderers,Discord.exe,19064,15660
LAPTOP-17497HSE,"C:\Program Files\WindowsApps\AppUp.IntelGraphicsExperience_1.100.2970.0_x64__8j3eq9eme6ctt\GCP.ML.BackgroundSysTray\IGCCTray.exe" ,IGCCTray.exe,9544,1308
LAPTOP-17497HSE,"C:\Program Files\WindowsApps\SpotifyAB.SpotifyMusic_1.145.621.0_x86__zpdnekdrzrea0\Spotify.exe" --type=crashpad-handler /prefetch:7 --max-uploads=5 --max-db-size=20 --max-db-age=5 --monitor-self-annotation=ptype=crashpad-handler "--database=C:\Users\hugox\AppData\Local\SpotifyAppX\User Data\Crashpad" "--metrics-dir=C:\Users\hugox\AppData\Local\SpotifyAppX\User Data" --url=https://crashdump.spotify.com:443/ --annotation=platform=win32 --annotation=product=spotify --annotation=version=1.1.45.621 --initial-client-data=0x5e0,0x5e4,0x5e8,0x5dc,0x5ec,0x796a5868,0x796a5878,0x796a5884,Spotify.exe,7220,19576
LAPTOP-17497HSE,"C:\Program Files\WindowsApps\SpotifyAB.SpotifyMusic_1.145.621.0_x86__zpdnekdrzrea0\Spotify.exe" --type=gpu-process --field-trial-handle=1800,3799741433551706843,16902123221973962558,131072 --enable-features=CastMediaRouteProvider --disable-d3d11 --log-file="C:\Program Files\WindowsApps\SpotifyAB.SpotifyMusic_1.145.621.0_x86__zpdnekdrzrea0\debug.log" --log-severity=disable --product-version=Spotify/1.1.45.621 --lang=fr --gpu-preferences=MAAAAAAAAADgAAAwAAAAAAAAAAAAAAAAAABgAAAAAAAQAAAAAAAAAAAAAAAAAAAAKAAAAAQAAAAgAAAAAAAAACgAAAAAAAAAMAAAAAAAAAA4AAAAAAAAABAAAAAAAAAAAAAAAAUAAAAQAAAAAAAAAAAAAAAGAAAAEAAAAAAAAAABAAAABQAAABAAAAAAAAAAAQAAAAYAAAA= --log-file="C:\Program Files\WindowsApps\SpotifyAB.SpotifyMusic_1.145.621.0_x86__zpdnekdrzrea0\debug.log" --mojo-platform-channel-handle=2096 /prefetch:2,Spotify.exe,7220,12256
LAPTOP-17497HSE,"C:\Program Files\WindowsApps\SpotifyAB.SpotifyMusic_1.145.621.0_x86__zpdnekdrzrea0\Spotify.exe" --type=utility --utility-sub-type=network.mojom.NetworkService --field-trial-handle=1800,3799741433551706843,16902123221973962558,131072 --enable-features=CastMediaRouteProvider --lang=fr --service-sandbox-type=network --log-file="C:\Program Files\WindowsApps\SpotifyAB.SpotifyMusic_1.145.621.0_x86__zpdnekdrzrea0\debug.log" --log-severity=disable --product-version=Spotify/1.1.45.621 --lang=fr --log-file="C:\Program Files\WindowsApps\SpotifyAB.SpotifyMusic_1.145.621.0_x86__zpdnekdrzrea0\debug.log" --mojo-platform-channel-handle=2768 /prefetch:8,Spotify.exe,7220,2704
LAPTOP-17497HSE,"C:\Program Files\WindowsApps\SpotifyAB.SpotifyMusic_1.145.621.0_x86__zpdnekdrzrea0\Spotify.exe" --type=renderer --log-file="C:\Program Files\WindowsApps\SpotifyAB.SpotifyMusic_1.145.621.0_x86__zpdnekdrzrea0\debug.log" --field-trial-handle=1800,3799741433551706843,16902123221973962558,131072 --enable-features=CastMediaRouteProvider --lang=fr --log-file="C:\Program Files\WindowsApps\SpotifyAB.SpotifyMusic_1.145.621.0_x86__zpdnekdrzrea0\debug.log" --log-severity=disable --product-version=Spotify/1.1.45.621 --disable-spell-checking --device-scale-factor=1.25 --num-raster-threads=4 --enable-main-frame-before-activation --renderer-client-id=4 --mojo-platform-channel-handle=2908 /prefetch:1,Spotify.exe,7220,9364
LAPTOP-17497HSE,"C:\Users\hugox\AppData\Local\Discord\app-0.0.307\Discord.exe" --type=utility --field-trial-handle=1544,10577206270723331433,14271523452381717022,131072 --disable-features=SpareRendererForSitePerProcess --lang=fr --service-sandbox-type=audio --service-request-channel-token=8477041436791389567 --mojo-platform-channel-handle=2824 /prefetch:8,Discord.exe,19064,10884
LAPTOP-17497HSE,"C:\Program Files\WindowsApps\AppUp.IntelGraphicsExperience_1.100.2970.0_x64__8j3eq9eme6ctt\IGCC.exe" -ServerName:App.AppXxq4ar3drev924dxqnatpa4s48c4zrxd1.mca,IGCC.exe,504,14568
LAPTOP-17497HSE,"C:\Program Files\WindowsApps\Microsoft.XboxGamingOverlay_5.420.10222.0_x64__8wekyb3d8bbwe\GameBar.exe" -ServerName:App.AppXbdkk0yrkwpcgeaem8zk81k8py1eaahny.mca,GameBar.exe,504,1428
LAPTOP-17497HSE,"C:\Program Files\WindowsApps\Microsoft.XboxGamingOverlay_5.420.10222.0_x64__8wekyb3d8bbwe\GameBarFTServer.exe" -Embedding,GameBarFTServer.exe,504,16776
LAPTOP-17497HSE,C:\Windows\System32\RuntimeBroker.exe -Embedding,RuntimeBroker.exe,504,3492
LAPTOP-17497HSE,"C:\Program Files (x86)\Brother\Brmfcmon\BrMfcWnd.exe" /AUTORUN,BrMfcWnd.exe,19252,18348
LAPTOP-17497HSE,"C:\Program Files (x86)\Brother\Brmfcmon\BrMfimon.exe",BrMfimon.exe,18348,14056
LAPTOP-17497HSE,"C:\Program Files (x86)\Brother\ControlCenter3\brccMCtl.exe" /autorun,BrccMCtl.exe,9624,10188
LAPTOP-17497HSE,"C:\Users\hugox\AppData\Local\Microsoft\Teams\current\Teams.exe" --type=renderer --autoplay-policy=no-user-gesture-required --disable-background-timer-throttling --field-trial-handle=1776,16690437217103656881,320088810334711641,131072 --enable-features=WebComponentsV0Enabled --disable-features=PictureInPicture,SpareRendererForSitePerProcess --lang=fr --enable-wer --app-user-model-id=com.squirrel.Teams.Teams --app-path="C:\Users\hugox\AppData\Local\Microsoft\Teams\current\resources\app.asar" --enable-sandbox --native-window-open --preload="C:\Users\hugox\AppData\Local\Microsoft\Teams\current\resources\app.asar\lib\renderer\experienceRenderer\preload_webview_sandbox.js" --background-color=#fff --guest-instance-id=5 --enable-blink-features --disable-blink-features --hidden-page --enable-websql --electron-shared-settings=eyJjci5jb21wYW55IjoiRWxlY3Ryb24iLCJjci5kdW1wcyI6IiIsImNyLmVuYWJsZWQiOmZhbHNlLCJjci5wcm9kdWN0IjoiRWxlY3Ryb24iLCJjci5zZXNzaW9uIjoiIiwiY3IudXJsIjoiIiwiY3IudmVyc2lvbiI6IiJ9 --device-scale-factor=1.25 --num-raster-threads=4 --enable-main-frame-before-activation --renderer-client-id=12 --no-v8-untrusted-code-mitigations --mojo-platform-channel-handle=4060 /prefetch:1 --msteams-process-type=experience-renderer,Teams.exe,15748,15280
LAPTOP-17497HSE,"C:\Users\hugox\AppData\Local\Microsoft\Teams\current\Teams.exe" --type=renderer --autoplay-policy=no-user-gesture-required --disable-background-timer-throttling --field-trial-handle=1776,16690437217103656881,320088810334711641,131072 --enable-features=WebComponentsV0Enabled --disable-features=PictureInPicture,SpareRendererForSitePerProcess --lang=fr --enable-wer --app-user-model-id=com.squirrel.Teams.Teams --app-path="C:\Users\hugox\AppData\Local\Microsoft\Teams\current\resources\app.asar" --no-sandbox --no-zygote --preload="C:\Users\hugox\AppData\Local\Microsoft\Teams\current\resources\app.asar\lib\pluginhost\preload.js" --background-color=#fff --enable-websql --electron-shared-settings=eyJjci5jb21wYW55IjoiRWxlY3Ryb24iLCJjci5kdW1wcyI6IiIsImNyLmVuYWJsZWQiOmZhbHNlLCJjci5wcm9kdWN0IjoiRWxlY3Ryb24iLCJjci5zZXNzaW9uIjoiIiwiY3IudXJsIjoiIiwiY3IudmVyc2lvbiI6IiJ9 --device-scale-factor=1.25 --num-raster-threads=4 --enable-main-frame-before-activation --renderer-client-id=14 --no-v8-untrusted-code-mitigations --mojo-platform-channel-handle=4332 /prefetch:1 --msteams-process-type=pluginHost,Teams.exe,15748,17900
LAPTOP-17497HSE,"C:\Program Files (x86)\Common Files\Wondershare\Wondershare Helper Compact\WSHelper.exe" ,WSHelper.exe,19252,16164
LAPTOP-17497HSE,C:\Windows\system32\svchost.exe -k UnistackSvcGroup,svchost.exe,988,7548
LAPTOP-17497HSE,"C:\Program Files\WindowsApps\Microsoft.WindowsStore_12011.1001.1.0_x64__8wekyb3d8bbwe\WinStore.App.exe" -ServerName:App.AppXc75wvwned5vhz4xyxxecvgdjhdkgsdza.mca,WinStore.App.exe,504,16492
LAPTOP-17497HSE,C:\Windows\System32\RuntimeBroker.exe -Embedding,RuntimeBroker.exe,504,17068
LAPTOP-17497HSE,"C:\Windows\ImmersiveControlPanel\SystemSettings.exe" -ServerName:microsoft.windows.immersivecontrolpanel,SystemSettings.exe,504,20584
LAPTOP-17497HSE,C:\Windows\System32\oobe\UserOOBEBroker.exe -Embedding,UserOOBEBroker.exe,504,16536
LAPTOP-17497HSE,SndVol.exe -m 69863124,SndVol.exe,9544,1116
LAPTOP-17497HSE,"C:\Program Files\WindowsApps\Microsoft.Windows.Photos_2020.20090.1002.0_x64__8wekyb3d8bbwe\Microsoft.Photos.exe" -ServerName:App.AppXzst44mncqdg84v7sv6p7yznqwssy6f7f.mca,Microsoft.Photos.exe,504,15400
LAPTOP-17497HSE,C:\Windows\System32\RuntimeBroker.exe -Embedding,RuntimeBroker.exe,504,3424
LAPTOP-17497HSE,"C:\Program Files (x86)\Microsoft\Edge\Application\msedge.exe" --profile-directory=Default,msedge.exe,9544,7784
LAPTOP-17497HSE,"C:\Program Files (x86)\Microsoft\Edge\Application\msedge.exe" --type=crashpad-handler "--user-data-dir=C:\Users\hugox\AppData\Local\Microsoft\Edge\User Data" /prefetch:7 --monitor-self-annotation=ptype=crashpad-handler "--database=C:\Users\hugox\AppData\Local\Microsoft\Edge\User Data\Crashpad" "--metrics-dir=C:\Users\hugox\AppData\Local\Microsoft\Edge\User Data" --annotation=IsOfficialBuild=1 --annotation=channel= --annotation=chromium-version=86.0.4240.198 "--annotation=exe=C:\Program Files (x86)\Microsoft\Edge\Application\msedge.exe" --annotation=plat=Win64 --annotation=prod=Edge --annotation=ver=86.0.622.69 --initial-client-data=0xe4,0xe8,0xec,0xc0,0xf0,0x7ff9463033c8,0x7ff9463033d8,0x7ff9463033e8,msedge.exe,7784,9612
LAPTOP-17497HSE,"C:\Program Files (x86)\Microsoft\Edge\Application\msedge.exe" --type=gpu-process --field-trial-handle=1580,5139099762899181767,4963882113965171918,131072 --gpu-preferences=MAAAAAAAAADgAAAwAAAAAAAAAAAAAAAAAABgAAAAAAAQAAAAAAAAAAAAAAAAAAAAKAAAAAQAAAAgAAAAAAAAACgAAAAAAAAAMAAAAAAAAAA4AAAAAAAAABAAAAAAAAAAAAAAAAUAAAAQAAAAAAAAAAAAAAAGAAAAEAAAAAAAAAABAAAABQAAABAAAAAAAAAAAQAAAAYAAAA= --mojo-platform-channel-handle=1820 /prefetch:2,msedge.exe,7784,8316
LAPTOP-17497HSE,"C:\Program Files (x86)\Microsoft\Edge\Application\msedge.exe" --type=utility --utility-sub-type=network.mojom.NetworkService --field-trial-handle=1580,5139099762899181767,4963882113965171918,131072 --lang=fr --service-sandbox-type=network --mojo-platform-channel-handle=2236 /prefetch:8,msedge.exe,7784,15452
LAPTOP-17497HSE,"C:\Program Files (x86)\Microsoft\Edge\Application\msedge.exe" --type=renderer --field-trial-handle=1580,5139099762899181767,4963882113965171918,131072 --lang=fr --disable-client-side-phishing-detection --device-scale-factor=1.25 --num-raster-threads=4 --enable-main-frame-before-activation --renderer-client-id=7 --no-v8-untrusted-code-mitigations --mojo-platform-channel-handle=3344 /prefetch:1,msedge.exe,7784,12212
LAPTOP-17497HSE,"C:\Program Files (x86)\Microsoft\Edge\Application\msedge.exe" --type=renderer --field-trial-handle=1580,5139099762899181767,4963882113965171918,131072 --lang=fr --extension-process --disable-client-side-phishing-detection --device-scale-factor=1.25 --num-raster-threads=4 --enable-main-frame-before-activation --renderer-client-id=5 --no-v8-untrusted-code-mitigations --mojo-platform-channel-handle=4628 /prefetch:1,msedge.exe,7784,7472
LAPTOP-17497HSE,"C:\Program Files (x86)\Microsoft\Edge\Application\msedge.exe" --type=renderer --field-trial-handle=1580,5139099762899181767,4963882113965171918,131072 --lang=fr --disable-client-side-phishing-detection --device-scale-factor=1.25 --num-raster-threads=4 --enable-main-frame-before-activation --renderer-client-id=12 --no-v8-untrusted-code-mitigations --mojo-platform-channel-handle=5512 /prefetch:1,msedge.exe,7784,9056
LAPTOP-17497HSE,"C:\Program Files (x86)\Microsoft\Edge\Application\msedge.exe" --type=renderer --field-trial-handle=1580,5139099762899181767,4963882113965171918,131072 --lang=fr --disable-client-side-phishing-detection --device-scale-factor=1.25 --num-raster-threads=4 --enable-main-frame-before-activation --renderer-client-id=13 --no-v8-untrusted-code-mitigations --mojo-platform-channel-handle=5876 /prefetch:1,msedge.exe,7784,5208
LAPTOP-17497HSE,"C:\Program Files (x86)\Microsoft\Edge\Application\msedge.exe" --type=renderer --field-trial-handle=1580,5139099762899181767,4963882113965171918,131072 --lang=fr --disable-client-side-phishing-detection --device-scale-factor=1.25 --num-raster-threads=4 --enable-main-frame-before-activation --renderer-client-id=14 --no-v8-untrusted-code-mitigations --mojo-platform-channel-handle=6148 /prefetch:1,msedge.exe,7784,8492
LAPTOP-17497HSE,"C:\Program Files (x86)\Microsoft\Edge\Application\msedge.exe" --type=renderer --field-trial-handle=1580,5139099762899181767,4963882113965171918,131072 --lang=fr --disable-client-side-phishing-detection --device-scale-factor=1.25 --num-raster-threads=4 --enable-main-frame-before-activation --renderer-client-id=15 --no-v8-untrusted-code-mitigations --mojo-platform-channel-handle=6132 /prefetch:1,msedge.exe,7784,18268
LAPTOP-17497HSE,"C:\Program Files (x86)\Microsoft\Edge\Application\msedge.exe" --type=renderer --field-trial-handle=1580,5139099762899181767,4963882113965171918,131072 --lang=fr --disable-client-side-phishing-detection --device-scale-factor=1.25 --num-raster-threads=4 --enable-main-frame-before-activation --renderer-client-id=17 --no-v8-untrusted-code-mitigations --mojo-platform-channel-handle=6652 /prefetch:1,msedge.exe,7784,21344
LAPTOP-17497HSE,"C:\Program Files (x86)\Microsoft\Edge\Application\msedge.exe" --type=renderer --field-trial-handle=1580,5139099762899181767,4963882113965171918,131072 --lang=fr --disable-client-side-phishing-detection --device-scale-factor=1.25 --num-raster-threads=4 --enable-main-frame-before-activation --renderer-client-id=28 --no-v8-untrusted-code-mitigations --mojo-platform-channel-handle=7852 /prefetch:1,msedge.exe,7784,16124
LAPTOP-17497HSE,"C:\Program Files (x86)\Microsoft\Edge\Application\msedge.exe" --type=renderer --field-trial-handle=1580,5139099762899181767,4963882113965171918,131072 --lang=fr --disable-client-side-phishing-detection --device-scale-factor=1.25 --num-raster-threads=4 --enable-main-frame-before-activation --renderer-client-id=29 --no-v8-untrusted-code-mitigations --mojo-platform-channel-handle=8156 /prefetch:1,msedge.exe,7784,11768
LAPTOP-17497HSE,"C:\Program Files (x86)\Microsoft\Edge\Application\msedge.exe" --type=renderer --field-trial-handle=1580,5139099762899181767,4963882113965171918,131072 --lang=fr --disable-client-side-phishing-detection --device-scale-factor=1.25 --num-raster-threads=4 --enable-main-frame-before-activation --renderer-client-id=31 --no-v8-untrusted-code-mitigations --mojo-platform-channel-handle=2948 /prefetch:1,msedge.exe,7784,20580
LAPTOP-17497HSE,"C:\Program Files (x86)\Microsoft\Edge\Application\msedge.exe" --type=renderer --field-trial-handle=1580,5139099762899181767,4963882113965171918,131072 --lang=fr --disable-client-side-phishing-detection --device-scale-factor=1.25 --num-raster-threads=4 --enable-main-frame-before-activation --renderer-client-id=32 --no-v8-untrusted-code-mitigations --mojo-platform-channel-handle=7228 /prefetch:1,msedge.exe,7784,5020
LAPTOP-17497HSE,"C:\Program Files (x86)\Microsoft\Edge\Application\msedge.exe" --type=utility --utility-sub-type=media.mojom.MediaService --field-trial-handle=1580,5139099762899181767,4963882113965171918,131072 --lang=fr --service-sandbox-type=mf_cdm --mojo-platform-channel-handle=7756 /prefetch:8,msedge.exe,7784,5608
LAPTOP-17497HSE,,mfpmp.exe,5608,16312
LAPTOP-17497HSE,,mfpmp.exe,5608,5716
LAPTOP-17497HSE,,mfpmp.exe,5608,19384
LAPTOP-17497HSE,"C:\Program Files (x86)\Microsoft\Edge\Application\msedge.exe" --type=utility --utility-sub-type=audio.mojom.AudioService --field-trial-handle=1580,5139099762899181767,4963882113965171918,131072 --lang=fr --service-sandbox-type=audio --mojo-platform-channel-handle=8224 /prefetch:8,msedge.exe,7784,11380
LAPTOP-17497HSE,"C:\Program Files (x86)\Microsoft\Edge\Application\msedge.exe" --type=renderer --field-trial-handle=1580,5139099762899181767,4963882113965171918,131072 --lang=fr --disable-client-side-phishing-detection --device-scale-factor=1.25 --num-raster-threads=4 --enable-main-frame-before-activation --renderer-client-id=39 --no-v8-untrusted-code-mitigations --mojo-platform-channel-handle=6252 /prefetch:1,msedge.exe,7784,18548
LAPTOP-17497HSE,"C:\Program Files (x86)\Microsoft\Edge\Application\msedge.exe" --type=renderer --field-trial-handle=1580,5139099762899181767,4963882113965171918,131072 --lang=fr --disable-client-side-phishing-detection --device-scale-factor=1.25 --num-raster-threads=4 --enable-main-frame-before-activation --renderer-client-id=41 --no-v8-untrusted-code-mitigations --mojo-platform-channel-handle=2364 /prefetch:1,msedge.exe,7784,2304
LAPTOP-17497HSE,,mfpmp.exe,5608,14172
LAPTOP-17497HSE,,mfpmp.exe,5608,21032
LAPTOP-17497HSE,,mfpmp.exe,5608,21416
LAPTOP-17497HSE,,mfpmp.exe,5608,9476
LAPTOP-17497HSE,,audiodg.exe,3212,19824
LAPTOP-17497HSE,,WmiPrvSE.exe,504,19200
LAPTOP-17497HSE,"C:\Program Files (x86)\Microsoft\Edge\Application\msedge.exe" --type=renderer --field-trial-handle=1580,5139099762899181767,4963882113965171918,131072 --lang=fr --disable-client-side-phishing-detection --device-scale-factor=1.25 --num-raster-threads=4 --enable-main-frame-before-activation --renderer-client-id=44 --no-v8-untrusted-code-mitigations --mojo-platform-channel-handle=8544 /prefetch:1,msedge.exe,7784,19056
LAPTOP-17497HSE,"C:\Program Files (x86)\Microsoft\Edge\Application\msedge.exe" --type=renderer --field-trial-handle=1580,5139099762899181767,4963882113965171918,131072 --lang=fr --disable-client-side-phishing-detection --device-scale-factor=1.25 --num-raster-threads=4 --enable-main-frame-before-activation --renderer-client-id=45 --no-v8-untrusted-code-mitigations --mojo-platform-channel-handle=8788 /prefetch:1,msedge.exe,7784,11448
LAPTOP-17497HSE,C:\Users\hugox\AppData\Local\Microsoft\OneDrive\20.169.0823.0008\FileCoAuth.exe -Embedding,FileCoAuth.exe,504,16432
LAPTOP-17497HSE,"C:\Windows\system32\NOTEPAD.EXE" C:\Users\hugox\OneDrive\Bureau\projetos.txt,notepad.exe,9544,2360
LAPTOP-17497HSE,"C:\Program Files (x86)\Microsoft\Edge\Application\msedge.exe" --type=renderer --field-trial-handle=1580,5139099762899181767,4963882113965171918,131072 --lang=fr --disable-client-side-phishing-detection --instant-process --device-scale-factor=1.25 --num-raster-threads=4 --enable-main-frame-before-activation --renderer-client-id=46 --no-v8-untrusted-code-mitigations --mojo-platform-channel-handle=7616 /prefetch:1,msedge.exe,7784,20900
LAPTOP-17497HSE,"C:\Program Files (x86)\Microsoft\Edge\Application\msedge.exe" --type=renderer --field-trial-handle=1580,5139099762899181767,4963882113965171918,131072 --lang=fr --disable-client-side-phishing-detection --device-scale-factor=1.25 --num-raster-threads=4 --enable-main-frame-before-activation --renderer-client-id=48 --no-v8-untrusted-code-mitigations --mojo-platform-channel-handle=8280 /prefetch:1,msedge.exe,7784,21264
LAPTOP-17497HSE,,svchost.exe,988,17988
LAPTOP-17497HSE,"C:\Program Files (x86)\Microsoft\Edge\Application\msedge.exe" --type=renderer --field-trial-handle=1580,5139099762899181767,4963882113965171918,131072 --lang=fr --disable-client-side-phishing-detection --device-scale-factor=1.25 --num-raster-threads=4 --enable-main-frame-before-activation --renderer-client-id=52 --no-v8-untrusted-code-mitigations --mojo-platform-channel-handle=3188 /prefetch:1,msedge.exe,7784,15420
LAPTOP-17497HSE,"C:\Program Files (x86)\Microsoft\Edge\Application\msedge.exe" --type=renderer --field-trial-handle=1580,5139099762899181767,4963882113965171918,131072 --lang=fr --disable-client-side-phishing-detection --device-scale-factor=1.25 --num-raster-threads=4 --enable-main-frame-before-activation --renderer-client-id=56 --no-v8-untrusted-code-mitigations --mojo-platform-channel-handle=9984 /prefetch:1,msedge.exe,7784,12860
LAPTOP-17497HSE,"C:\Program Files (x86)\Microsoft\Edge\Application\msedge.exe" --type=renderer --field-trial-handle=1580,5139099762899181767,4963882113965171918,131072 --lang=fr --disable-client-side-phishing-detection --device-scale-factor=1.25 --num-raster-threads=4 --enable-main-frame-before-activation --renderer-client-id=57 --no-v8-untrusted-code-mitigations --mojo-platform-channel-handle=8576 /prefetch:1,msedge.exe,7784,292
LAPTOP-17497HSE,"C:\Program Files (x86)\Microsoft\Edge\Application\msedge.exe" --type=renderer --field-trial-handle=1580,5139099762899181767,4963882113965171918,131072 --lang=fr --disable-client-side-phishing-detection --device-scale-factor=1.25 --num-raster-threads=4 --enable-main-frame-before-activation --renderer-client-id=58 --no-v8-untrusted-code-mitigations --mojo-platform-channel-handle=6908 /prefetch:1,msedge.exe,7784,9440
LAPTOP-17497HSE,C:\Windows\System32\CompPkgSrv.exe -Embedding,CompPkgSrv.exe,504,18244
LAPTOP-17497HSE,"C:\Windows\SystemApps\InputApp_cw5n1h2txyewy\WindowsInternal.ComposableShell.Experiences.TextInput.InputApp.exe" -ServerName:App.AppXagta193n5rpf7mheremt3yyfa1g555vc.mca,WindowsInternal.ComposableShell.Experiences.TextInput.InputApp.exe,504,19512
LAPTOP-17497HSE,"C:\Program Files (x86)\Microsoft\Edge\Application\msedge.exe" --type=renderer --field-trial-handle=1580,5139099762899181767,4963882113965171918,131072 --lang=fr --disable-client-side-phishing-detection --device-scale-factor=1.25 --num-raster-threads=4 --enable-main-frame-before-activation --renderer-client-id=60 --no-v8-untrusted-code-mitigations --mojo-platform-channel-handle=9584 /prefetch:1,msedge.exe,7784,7392
LAPTOP-17497HSE,"C:\Users\hugox\AppData\Local\Microsoft\Teams\current\Teams.exe" --type=renderer --autoplay-policy=no-user-gesture-required --disable-background-timer-throttling --field-trial-handle=1776,16690437217103656881,320088810334711641,131072 --enable-features=WebComponentsV0Enabled --disable-features=PictureInPicture,SpareRendererForSitePerProcess --lang=fr --enable-wer --app-user-model-id=com.squirrel.Teams.Teams --app-path="C:\Users\hugox\AppData\Local\Microsoft\Teams\current\resources\app.asar" --electron-shared-settings=eyJjci5jb21wYW55IjoiRWxlY3Ryb24iLCJjci5kdW1wcyI6IiIsImNyLmVuYWJsZWQiOmZhbHNlLCJjci5wcm9kdWN0IjoiRWxlY3Ryb24iLCJjci5zZXNzaW9uIjoiIiwiY3IudXJsIjoiIiwiY3IudmVyc2lvbiI6IiJ9 --device-scale-factor=1.25 --num-raster-threads=4 --enable-main-frame-before-activation --renderer-client-id=154 --no-v8-untrusted-code-mitigations --mojo-platform-channel-handle=4632 /prefetch:1,Teams.exe,15748,18356
LAPTOP-17497HSE,"C:\Windows\system32\backgroundTaskHost.exe" -ServerName:CortanaUI.AppXy7vb4pc2dr3kc93kfc509b1d0arkfb2x.mca,backgroundTaskHost.exe,504,4376
LAPTOP-17497HSE,C:\Windows\System32\smartscreen.exe -Embedding,smartscreen.exe,504,13172
LAPTOP-17497HSE,"C:\Windows\system32\cmd.exe" ,cmd.exe,9544,8800
LAPTOP-17497HSE,\??\C:\Windows\system32\conhost.exe 0x4,conhost.exe,8800,17500
LAPTOP-17497HSE,"C:\Windows\system32\SearchProtocolHost.exe" Global\UsGthrFltPipeMssGthrPipe_S-1-5-21-4104557157-3763362035-3222392551-100179_ Global\UsGthrCtrlFltPipeMssGthrPipe_S-1-5-21-4104557157-3763362035-3222392551-100179 1 -2147483646 "Software\Microsoft\Windows Search" "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT; MS Search 4.0 Robot)" "C:\ProgramData\Microsoft\Search\Data\Temp\usgthrsvc" "DownLevelDaemon"  "1",SearchProtocolHost.exe,14736,3136
LAPTOP-17497HSE,,SearchFilterHost.exe,14736,19932
LAPTOP-17497HSE,wmic  process get description,processid,parentprocessid,commandline /format:csv,WMIC.exe,8800,7876
```

- 5 services système
- remote registry , ce service système sers aux utilisateur de modifier à distance les paramètre de registre de l'ordinateur.
- sticvc , ce service système sers à fournir des services d’acquisition d’images pour les scanners et les appareils photo.
- Messenger , ce service système sers à envoyer et recevoir des messages des services d’alertes entre les clients et les serveurs
- helpsvc , ce service système permet l’application Aide et support de fonctionner sur cet ordinateur.
- AudioSrv , ce service système gère les périphériques audio pour les programmes basés sur Windows.

## Network

- cartes réseaux

```
 Get-NetAdapter -Physical

Name                      InterfaceDescription                    ifIndex Status       MacAddress             LinkSpeed
----                      --------------------                    ------- ------       ----------             ---------
Wi-Fi                     Intel(R) Wi-Fi 6 AX201 160MHz                17 Up           C8-58-C0-24-71-7E     144.4 Mbps
Ethernet 2                Realtek PCIe GbE Family Controller           13 Disconnected 98-BB-1E-1C-D5-7F          0 bps
```

La carte Intel(R) Wi-Fi 6 AX201 160MHz permet la connexion au réseau sans fil wifi et la carte Realtek PCIe GbE Family Controller permet de se connecté au réseau par liaison fillaire .

- Lister tous les ports TCP et UDP en utilisation

```
 netstat -ab

Connexions actives

  Proto  Adresse locale         Adresse distante       État
  TCP    0.0.0.0:135            LAPTOP-17497HSE:0      LISTENING
  RpcEptMapper
 [svchost.exe]
  TCP    0.0.0.0:445            LAPTOP-17497HSE:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:1337           LAPTOP-17497HSE:0      LISTENING
 [RzSDKServer.exe]
  TCP    0.0.0.0:5040           LAPTOP-17497HSE:0      LISTENING
  CDPSvc
 [svchost.exe]
  TCP    0.0.0.0:5426           LAPTOP-17497HSE:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:49664          LAPTOP-17497HSE:0      LISTENING
 [lsass.exe]
  TCP    0.0.0.0:49665          LAPTOP-17497HSE:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:49666          LAPTOP-17497HSE:0      LISTENING
  EventLog
 [svchost.exe]
  TCP    0.0.0.0:49667          LAPTOP-17497HSE:0      LISTENING
  Schedule
 [svchost.exe]
  TCP    0.0.0.0:49668          LAPTOP-17497HSE:0      LISTENING
 [spoolsv.exe]
  TCP    0.0.0.0:49670          LAPTOP-17497HSE:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:54235          LAPTOP-17497HSE:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:54236          LAPTOP-17497HSE:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:57621          LAPTOP-17497HSE:0      LISTENING
 [Spotify.exe]
  TCP    0.0.0.0:61066          LAPTOP-17497HSE:0      LISTENING
 [Spotify.exe]
  TCP    127.0.0.1:6463         LAPTOP-17497HSE:0      LISTENING
 [Discord.exe]
  TCP    127.0.0.1:13339        LAPTOP-17497HSE:0      LISTENING
 [RzSDKServer.exe]
  TCP    127.0.0.1:60896        LAPTOP-17497HSE:65001  ESTABLISHED
 [nvcontainer.exe]
  TCP    127.0.0.1:60949        LAPTOP-17497HSE:0      LISTENING
 [NVIDIA Web Helper.exe]
  TCP    127.0.0.1:65001        LAPTOP-17497HSE:0      LISTENING
 [nvcontainer.exe]
  TCP    127.0.0.1:65001        LAPTOP-17497HSE:60896  ESTABLISHED
 [nvcontainer.exe]
  TCP    192.168.1.27:139       LAPTOP-17497HSE:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    192.168.1.27:60924     40.67.254.36:https     ESTABLISHED
  WpnService
 [svchost.exe]
  TCP    192.168.1.27:61069     127:4070               ESTABLISHED
 [Spotify.exe]
  TCP    192.168.1.27:61075     47:https               ESTABLISHED
 [Spotify.exe]
  TCP    192.168.1.27:61173     52.114.75.169:https    ESTABLISHED
 [Teams.exe]
  TCP    192.168.1.27:61459     a23-40-114-33:https    CLOSE_WAIT
 [WinStore.App.exe]
  TCP    192.168.1.27:61460     a23-40-114-33:https    CLOSE_WAIT
 [WinStore.App.exe]
  TCP    192.168.1.27:61461     a23-40-114-33:https    CLOSE_WAIT
 [WinStore.App.exe]
  TCP    192.168.1.27:61462     a23-40-114-33:https    CLOSE_WAIT
 [WinStore.App.exe]
  TCP    192.168.1.27:61463     a23-40-114-33:https    CLOSE_WAIT
 [WinStore.App.exe]
  TCP    192.168.1.27:61464     a23-40-114-33:https    CLOSE_WAIT
 [WinStore.App.exe]
  TCP    192.168.1.27:61466     a23-40-113-188:http    CLOSE_WAIT
 [WinStore.App.exe]
  TCP    192.168.1.27:61467     a23-40-113-188:http    CLOSE_WAIT
 [WinStore.App.exe]
  TCP    192.168.1.27:61468     a23-40-113-188:http    CLOSE_WAIT
 [WinStore.App.exe]
  TCP    192.168.1.27:61469     a23-40-113-188:http    CLOSE_WAIT
 [WinStore.App.exe]
  TCP    192.168.1.27:61470     a23-40-113-188:http    CLOSE_WAIT
 [WinStore.App.exe]
  TCP    192.168.1.27:61472     a23-40-114-33:https    CLOSE_WAIT
 [WinStore.App.exe]
  TCP    192.168.1.27:61474     a23-40-113-188:http    CLOSE_WAIT
 [WinStore.App.exe]
  TCP    192.168.1.27:61475     a23-40-113-188:http    CLOSE_WAIT
 [WinStore.App.exe]
  TCP    192.168.1.27:61476     a23-40-112-10:http     CLOSE_WAIT
 [WinStore.App.exe]
  TCP    192.168.1.27:61486     a23-40-114-33:https    CLOSE_WAIT
 [WinStore.App.exe]
  TCP    192.168.1.27:61611     13.107.5.88:https      ESTABLISHED
 [Microsoft.Photos.exe]
  TCP    192.168.1.27:61788     52.114.88.10:https     ESTABLISHED
 [Teams.exe]
  TCP    192.168.1.27:61911     40.67.251.132:https    ESTABLISHED
 [msedge.exe]
  TCP    192.168.1.27:62544     162.159.133.234:https  ESTABLISHED
 [Discord.exe]
  TCP    192.168.1.27:62554     ec2-18-211-21-156:https  ESTABLISHED
 [RazerCentralService.exe]
  TCP    192.168.1.27:62779     40.67.254.36:https     ESTABLISHED
 [OneDrive.exe]
  TCP    192.168.1.27:62888     ec2-52-69-151-241:https  ESTABLISHED
 [msedge.exe]
  TCP    192.168.1.27:63456     25:https               ESTABLISHED
 [Spotify.exe]
  TCP    192.168.1.27:63469     ec2-54-74-73-31:https  ESTABLISHED
 [msedge.exe]
  TCP    192.168.1.27:63586     ec2-54-194-157-226:https  ESTABLISHED
 [msedge.exe]
  TCP    192.168.1.27:63596     204.79.197.222:https   ESTABLISHED
 [SearchUI.exe]
  TCP    192.168.1.27:63650     104.17.1.164:https     ESTABLISHED
 [msedge.exe]
  TCP    192.168.1.27:63654     par10s33-in-f2:https   ESTABLISHED
 [msedge.exe]
  TCP    192.168.1.27:63658     ec2-34-230-93-158:https  CLOSE_WAIT
 [EpicGamesLauncher.exe]
  TCP    192.168.1.27:63661     server-99-86-89-49:https  ESTABLISHED
 [msedge.exe]
  TCP    192.168.1.27:63662     13.107.43.15:https     ESTABLISHED
 [XboxAppServices.exe]
  TCP    192.168.1.27:63663     ec2-3-218-125-188:https  ESTABLISHED
 [msedge.exe]
  TCP    192.168.1.27:63664     ec2-3-218-125-188:https  ESTABLISHED
 [msedge.exe]
  TCP    192.168.1.27:63665     ec2-34-249-233-66:https  ESTABLISHED
 [msedge.exe]
  TCP    192.168.1.27:63667     52.113.205.27:https    ESTABLISHED
 [Teams.exe]
  TCP    192.168.1.27:63668     a-0001:https           ESTABLISHED
 [SearchUI.exe]
  TCP    192.168.1.27:63669     52.97.150.2:https      ESTABLISHED
 [SearchUI.exe]
  TCP    192.168.1.27:63670     51.140.157.153:https   ESTABLISHED
 [SearchUI.exe]
  TCP    192.168.1.27:63672     13.107.136.254:https   ESTABLISHED
 [SearchUI.exe]
  TCP    192.168.1.27:63673     13.107.246.10:https    ESTABLISHED
 [SearchUI.exe]
  TCP    192.168.1.27:63674     bingforbusiness:https  ESTABLISHED
 [SearchUI.exe]
  TCP    192.168.1.27:63675     a23-54-60-66:https     ESTABLISHED
 [SearchUI.exe]
  TCP    192.168.1.27:63676     13.107.3.254:https     ESTABLISHED
 [SearchUI.exe]
  TCP    192.168.1.27:63677     40.90.22.185:https     ESTABLISHED
  wlidsvc
 [svchost.exe]
  TCP    192.168.56.1:139       LAPTOP-17497HSE:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    192.168.120.2:139      LAPTOP-17497HSE:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    [::]:135               LAPTOP-17497HSE:0      LISTENING
  RpcEptMapper
 [svchost.exe]
  TCP    [::]:445               LAPTOP-17497HSE:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    [::]:1337              LAPTOP-17497HSE:0      LISTENING
 [RzSDKServer.exe]
  TCP    [::]:5426              LAPTOP-17497HSE:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    [::]:49664             LAPTOP-17497HSE:0      LISTENING
 [lsass.exe]
  TCP    [::]:49665             LAPTOP-17497HSE:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    [::]:49666             LAPTOP-17497HSE:0      LISTENING
  EventLog
 [svchost.exe]
  TCP    [::]:49667             LAPTOP-17497HSE:0      LISTENING
  Schedule
 [svchost.exe]
  TCP    [::]:49668             LAPTOP-17497HSE:0      LISTENING
 [spoolsv.exe]
  TCP    [::]:49670             LAPTOP-17497HSE:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    [::]:54235             LAPTOP-17497HSE:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    [::]:54236             LAPTOP-17497HSE:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    [::1]:5426             LAPTOP-17497HSE:60909  ESTABLISHED
 Impossible d’obtenir les informations de propriétaire
  TCP    [::1]:5426             LAPTOP-17497HSE:60913  ESTABLISHED
 Impossible d’obtenir les informations de propriétaire
  TCP    [::1]:5426             LAPTOP-17497HSE:60916  ESTABLISHED
 Impossible d’obtenir les informations de propriétaire
  TCP    [::1]:5426             LAPTOP-17497HSE:60919  ESTABLISHED
 Impossible d’obtenir les informations de propriétaire
  TCP    [::1]:5426             LAPTOP-17497HSE:60953  ESTABLISHED
 Impossible d’obtenir les informations de propriétaire
  TCP    [::1]:5426             LAPTOP-17497HSE:60995  ESTABLISHED
 Impossible d’obtenir les informations de propriétaire
  TCP    [::1]:5426             LAPTOP-17497HSE:61001  ESTABLISHED
 Impossible d’obtenir les informations de propriétaire
  TCP    [::1]:5426             LAPTOP-17497HSE:61004  ESTABLISHED
 Impossible d’obtenir les informations de propriétaire
  TCP    [::1]:5426             LAPTOP-17497HSE:61010  ESTABLISHED
 Impossible d’obtenir les informations de propriétaire
  TCP    [::1]:5426             LAPTOP-17497HSE:61015  ESTABLISHED
 Impossible d’obtenir les informations de propriétaire
  TCP    [::1]:5426             LAPTOP-17497HSE:61016  ESTABLISHED
 Impossible d’obtenir les informations de propriétaire
  TCP    [::1]:5426             LAPTOP-17497HSE:61024  ESTABLISHED
 Impossible d’obtenir les informations de propriétaire
  TCP    [::1]:5426             LAPTOP-17497HSE:61029  ESTABLISHED
 Impossible d’obtenir les informations de propriétaire
  TCP    [::1]:13337            LAPTOP-17497HSE:0      LISTENING
 [RzSDKServer.exe]
  TCP    [::1]:49669            LAPTOP-17497HSE:0      LISTENING
 [jhi_service.exe]
  TCP    [::1]:60909            LAPTOP-17497HSE:5426   ESTABLISHED
 [Razer Synapse Service Process.exe]
  TCP    [::1]:60913            LAPTOP-17497HSE:5426   ESTABLISHED
 [Razer Synapse Service Process.exe]
  TCP    [::1]:60916            LAPTOP-17497HSE:5426   ESTABLISHED
 [Razer Synapse Service Process.exe]
  TCP    [::1]:60919            LAPTOP-17497HSE:5426   ESTABLISHED
 [Razer Synapse Service Process.exe]
  TCP    [::1]:60953            LAPTOP-17497HSE:5426   ESTABLISHED
 [Razer Synapse 3.exe]
  TCP    [::1]:60995            LAPTOP-17497HSE:5426   ESTABLISHED
 [Razer Synapse 3.exe]
  TCP    [::1]:61001            LAPTOP-17497HSE:5426   ESTABLISHED
 [Razer Synapse 3.exe]
  TCP    [::1]:61004            LAPTOP-17497HSE:5426   ESTABLISHED
 [Razer Synapse 3.exe]
  TCP    [::1]:61010            LAPTOP-17497HSE:5426   ESTABLISHED
 [Razer Synapse 3.exe]
  TCP    [::1]:61015            LAPTOP-17497HSE:5426   ESTABLISHED
 [Razer Synapse 3.exe]
  TCP    [::1]:61016            LAPTOP-17497HSE:5426   ESTABLISHED
 [Razer Synapse 3.exe]
  TCP    [::1]:61024            LAPTOP-17497HSE:5426   ESTABLISHED
 [Razer Synapse 3.exe]
  TCP    [::1]:61029            LAPTOP-17497HSE:5426   ESTABLISHED
 [Razer Synapse 3.exe]
  UDP    0.0.0.0:1900           *:*
 [Spotify.exe]
  UDP    0.0.0.0:1900           *:*
 [Spotify.exe]
  UDP    0.0.0.0:1900           *:*
 [Spotify.exe]
  UDP    0.0.0.0:5050           *:*
  CDPSvc
 [svchost.exe]
  UDP    0.0.0.0:5353           *:*
 [msedge.exe]
  UDP    0.0.0.0:5353           *:*
 [msedge.exe]
  UDP    0.0.0.0:5353           *:*
 [msedge.exe]
  UDP    0.0.0.0:5353           *:*
 [Spotify.exe]
  UDP    0.0.0.0:5353           *:*
 [Spotify.exe]
  UDP    0.0.0.0:5353           *:*
 [Spotify.exe]
  UDP    0.0.0.0:5353           *:*
 [Spotify.exe]
  UDP    0.0.0.0:5353           *:*
 [Spotify.exe]
  UDP    0.0.0.0:5353           *:*
 [Spotify.exe]
  UDP    0.0.0.0:5353           *:*
 [Spotify.exe]
  UDP    0.0.0.0:5353           *:*
 [msedge.exe]
  UDP    0.0.0.0:5353           *:*
 [Spotify.exe]
  UDP    0.0.0.0:5353           *:*
 [Spotify.exe]
  UDP    0.0.0.0:5353           *:*
  Dnscache
 [svchost.exe]
  UDP    0.0.0.0:5353           *:*
 [msedge.exe]
  UDP    0.0.0.0:5353           *:*
 [msedge.exe]
  UDP    0.0.0.0:5355           *:*
  Dnscache
 [svchost.exe]
  UDP    0.0.0.0:53299          *:*
 [Teams.exe]
  UDP    0.0.0.0:54925          *:*
  stisvc
 [svchost.exe]
  UDP    0.0.0.0:55151          *:*
 [Teams.exe]
  UDP    0.0.0.0:56167          *:*
 [msedge.exe]
  UDP    0.0.0.0:57621          *:*
 [Spotify.exe]
  UDP    0.0.0.0:60097          *:*
 [nvcontainer.exe]
  UDP    127.0.0.1:1900         *:*
  SSDPSRV
 [svchost.exe]
  UDP    127.0.0.1:10060        *:*
 [NVIDIA Web Helper.exe]
  UDP    127.0.0.1:56413        *:*
  SSDPSRV
 [svchost.exe]
  UDP    127.0.0.1:59204        *:*
  iphlpsvc
 [svchost.exe]
  UDP    127.0.0.1:59675        *:*
 [nvcontainer.exe]
  UDP    192.168.1.27:137       *:*
 Impossible d’obtenir les informations de propriétaire
  UDP    192.168.1.27:138       *:*
 Impossible d’obtenir les informations de propriétaire
  UDP    192.168.1.27:1900      *:*
  SSDPSRV
 [svchost.exe]
  UDP    192.168.1.27:2177      *:*
  QWAVE
 [svchost.exe]
  UDP    192.168.1.27:5353      *:*
 [nvcontainer.exe]
  UDP    192.168.1.27:56412     *:*
  SSDPSRV
 [svchost.exe]
  UDP    192.168.56.1:137       *:*
 Impossible d’obtenir les informations de propriétaire
  UDP    192.168.56.1:138       *:*
 Impossible d’obtenir les informations de propriétaire
  UDP    192.168.56.1:1900      *:*
  SSDPSRV
 [svchost.exe]
  UDP    192.168.56.1:2177      *:*
  QWAVE
 [svchost.exe]
  UDP    192.168.56.1:5353      *:*
 [nvcontainer.exe]
  UDP    192.168.56.1:56410     *:*
  SSDPSRV
 [svchost.exe]
  UDP    192.168.120.2:137      *:*
 Impossible d’obtenir les informations de propriétaire
  UDP    192.168.120.2:138      *:*
 Impossible d’obtenir les informations de propriétaire
  UDP    192.168.120.2:1900     *:*
  SSDPSRV
 [svchost.exe]
  UDP    192.168.120.2:2177     *:*
  QWAVE
 [svchost.exe]
  UDP    192.168.120.2:5353     *:*
 [nvcontainer.exe]
  UDP    192.168.120.2:56411    *:*
  SSDPSRV
 [svchost.exe]
  UDP    [::]:5353              *:*
 [msedge.exe]
  UDP    [::]:5353              *:*
 [msedge.exe]
  UDP    [::]:5353              *:*
 [Spotify.exe]
  UDP    [::]:5353              *:*
 [Spotify.exe]
  UDP    [::]:5353              *:*
 [Spotify.exe]
  UDP    [::]:5353              *:*
  Dnscache
 [svchost.exe]
  UDP    [::]:5353              *:*
 [msedge.exe]
  UDP    [::]:5355              *:*
  Dnscache
 [svchost.exe]
  UDP    [::]:53299             *:*
 [Teams.exe]
  UDP    [::]:55151             *:*
 [Teams.exe]
  UDP    [::]:60098             *:*
 [nvcontainer.exe]
  UDP    [::1]:1900             *:*
  SSDPSRV
 [svchost.exe]
  UDP    [::1]:5353             *:*
 [nvcontainer.exe]
  UDP    [::1]:56409            *:*
  SSDPSRV
 [svchost.exe]
  UDP    [fe80::31c8:8828:b09e:658c%17]:1900  *:*
  SSDPSRV
 [svchost.exe]
  UDP    [fe80::31c8:8828:b09e:658c%17]:2177  *:*
  QWAVE
 [svchost.exe]
  UDP    [fe80::31c8:8828:b09e:658c%17]:56408  *:*
  SSDPSRV
 [svchost.exe]
  UDP    [fe80::417f:2287:1af2:d907%5]:1900  *:*
  SSDPSRV
 [svchost.exe]
  UDP    [fe80::417f:2287:1af2:d907%5]:2177  *:*
  QWAVE
 [svchost.exe]
  UDP    [fe80::417f:2287:1af2:d907%5]:56407  *:*
  SSDPSRV
 [svchost.exe]
  UDP    [fe80::c50d:b208:502f:e6e2%18]:1900  *:*
  SSDPSRV
 [svchost.exe]
  UDP    [fe80::c50d:b208:502f:e6e2%18]:2177  *:*
  QWAVE
 [svchost.exe]
  UDP    [fe80::c50d:b208:502f:e6e2%18]:56406  *:*
  SSDPSRV
 [svchost.exe]
```

## Scripting

- scrip 1
  (j'ai réaliser les sripts sur powershell ISE)

```
#affichage nom de la machine
hostname

#ip principale
Get-NetIPAddress -PrefixOrigin Dhcp

#os et version de l'os
Get-ComputerInfo -Property "OsName"


#date et heure d'allumage
Get-CimInstance -ClassName Win32_OperatingSystem | Select CSName, LastBootUpTime

#détermine si l'os est à jour
Get-ComputerInfo -Property OsServicePackminorVersion

#Espace RAM utilisé / Espace RAM dispo
Get-WmiObject -Class Win32_OperatingSystem | Format-List TotalVisibleMemorySize,FreePhysicalMemory

#Espace disque utilisé / Espace disque dispo
Get-WmiObject Win32_LogicalDisk

#liste les utilisateurs de la machine
net user

#calcul et affiche le temps de réponse moyen
ping 8.8.8.8
```

- script 2

```
Param ([string] $action, [int] $x)
if ($action -eq "lock" )
{Start-Sleep -Seconds $x
Invoke-Command {rundll32.exe user32.dll,LockWorkStation}}
if ($action -eq "shutdown" )
{Start-Sleep -Seconds $x
Stop-Computer -force}
```

## Gestion des softs

- l'intéret de l'utilisation d'un gestionnaire de paquets

Par rapport au téléchargement en direct sur internet c'est plus sécuriser et plus rapide d'utiliser un gestionnaire de paquets
Les personnes impliqué dans le téléchargement sont le client , l'hébergeur du site et le créateur de l'application

## Machine virtuelle

![](https://i.imgur.com/09C12C7.png)
