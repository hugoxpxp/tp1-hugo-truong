﻿#affichage nom de la machine 
hostname

#ip principale 
Get-NetIPAddress -PrefixOrigin Dhcp

#os et version de l'os
Get-ComputerInfo -Property "OsName"


#date et heure d'allumage
Get-CimInstance -ClassName Win32_OperatingSystem | Select CSName, LastBootUpTime

#détermine si l'os est à jour 
 Get-ComputerInfo -Property OsServicePackminorVersion

#Espace RAM utilisé / Espace RAM dispo
Get-WmiObject -Class Win32_OperatingSystem | Format-List TotalVisibleMemorySize,FreePhysicalMemory

#Espace disque utilisé / Espace disque dispo
Get-WmiObject Win32_LogicalDisk

#liste les utilisateurs de la machine
net user
  
#calcul et affiche le temps de réponse moyen 
ping 8.8.8.8